import Component from '@ember/component';
import layout from './template';

export default Component.extend({
  tagName: "p",
  classNames: ["color--white", "style--italic"],
  message:null,
  layout
});
