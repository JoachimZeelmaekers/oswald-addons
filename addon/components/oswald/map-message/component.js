import Component from '@ember/component';
import layout from './template';
import { computed } from '@ember/object';
import { inject } from '@ember/service';

export default Component.extend({
    tagName: "p",
    classNames: ["color--white", "style--italic"],
    oswaldMapService: inject(),
    key: computed.alias('oswaldMapService.googleMapsApiKey'),

    mapsUrl: computed('message', function () {
        let message = this.get('message');
        let baseUrl = "https://www.google.com/maps/embed/v1/";

        if (message.elements.latitude && message.elements.longitude) {
            baseUrl += "view?zoom=12&center=";
            baseUrl += message.elements.latitude + "," + message.elements.longitude;
        } else if (message.elements.locationQuery) {
            baseUrl += "search?q=" + message.elements.locationQuery;
        } else {
            return null;
        }
        baseUrl += "&key=" + this.get('key');

        return baseUrl;
    }),
    layout
});
