import Component from '@ember/component';
import layout from '../templates/components/message-balloon';
import { computed } from '@ember/object';
export default Component.extend({
  tagName: "div",
  classNameBindings: ["oswaldBubbleLeftInner", 'isQuickreply:quickreply', 'isPrimaryColor:primaryColor'],

  isPrimaryColor: computed('message', function () {
    if (this.get('message').url) return false;
    return true;
  }),

  oswaldBubbleLeftInner: computed('message', function () {
    if (this.get('message').url) return false;
    return true;
  }),

  isQuickreply: computed('message', function () {
    if (this.get('message').url) return true;
    return false;
  }),
  layout,
  actions: {
    pickQuickResponse(event) {
      this.pickQuickResponse(event);
    },
    sendMessage(event) {
      this.sendMessage(event);
    }
  }
});
