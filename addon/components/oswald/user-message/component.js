import Component from '@ember/component';
import layout from './template';

export default Component.extend({
  tagName:'div',
  classNames:['oswald-bubble-right','secondaryColor'],
  layout,
  message:null,
});
