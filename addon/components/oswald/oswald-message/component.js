import Component from '@ember/component';
import layout from './template';
import { computed, get } from '@ember/object';
import { alias } from '@ember/object/computed';

export default Component.extend({
    layout,
    chatbot: null,
    resultItem: null,
    data: alias('resultItem.data'),
    label: alias("resultItem.metadata.label"),
    showQuickReplies: true,

    allVisible: computed("data.@each.visible", function () {
        return !this.get("data").find(function (item) {
            return !get(item, "visible");
        });
    }),

    actions: {
        pickQuickResponse: function (message) {
            this.set('showQuickReplies', false);
            this.sendMessage(message);
        },
        sendMessage: function (message) {
            this.sendMessage(message);
        }
    }
});
