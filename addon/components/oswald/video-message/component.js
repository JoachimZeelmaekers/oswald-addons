import Component from '@ember/component';
import layout from './template';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: "p",
  classNames: ["color--white", "style--italic"],
  message: null,
  url: computed('message', function () {
    let message = this.get('message');
    return message.url.replace('watch?v=', 'embed/');
  }),
  allowFullScreen: computed('message', function () {
    let message = this.get('message');
    if (message.elements && message.elements.hasOwnProperty('allowFullScreen') && message.elements.allowFullScreen) {
      return true;
    } else {
      return false;
    }
  }),
  layout
});
