
import layout from '../templates/components/oswald-messenger';
import Component from '@ember/component';
import $ from 'jquery';
import { computed, get, set } from '@ember/object';
import { later } from '@ember/runloop';

export default Component.extend({
  layout,
  tagName: 'div',
  classNames: ['oswald-text'],
  chatbot: null,
  allResults: null,
  lastResultInfo: computed.alias("results"),

  delay: computed('chatbot', function () {
    let chatbot = this.get('chatbot');
    if (chatbot && chatbot.widget && chatbot.widget.messageTimeout) {
      return chatbot.widget.messageTimeout;
    }
    return 1500;
  }),

  scrollToBottom() {
    var conversation = $(".oswald-text");
    conversation.animate(
      {
        scrollTop: conversation[0].scrollHeight + 100
      },
      500
    );
  },

  invisibleResults: computed("allResults.length", function () {
    let results = this.get("allResults");
    let invisible = [];

    if (results) {
      results.map(item => {
        if (item.data) {
          item.data.map(message => {
            if (!message.visible) {
              invisible.push(message);
            }
          });
        }
      });
    }
    return invisible;
  }),

  init: function () {
    this._super(...arguments);
    this.allResults = [];
  },

  didInsertElement: function () {
    this._super(...arguments);
    this.scrollToBottom();
    this.set(
      "loopy",
      setInterval(() => {
        let target = this.get("invisibleResults").objectAt(0);
        if (target && !get(target, "visible")) {
          set(target, "visible", true);
          this.notifyPropertyChange("invisibleResults");
          this.checkIfInvisibleResults();
          later(this, () => {
            this.scrollToBottom();
          }, 100);
        }
      }, this.get("delay"))
    );
    this.checkIfInvisibleResults();
  },

  didRender() {
    var input = $("#message");
    if (input) {
      input.focus();
    }
  },

  willDestroyElement: function () {
    clearInterval(this.get("loopy"));
  },

  checkIfInvisibleResults: function() {
    if(this.get('invisibleResults').length == 0 ) {
      this.set('typing', false);
      this.notifyPropertyChange('typing');
    }
  },

  actions: {
    sendMessage(message) {
      this.sendMessage(message);
    }
  }
});
