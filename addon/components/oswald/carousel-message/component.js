import Component from '@ember/component';
import layout from './template';

export default Component.extend({
    tagName: "p",
    classNames: ["color--white", "style--italic"],
    message:null,
    actions:{
        selectedItem(button) {
            if(button.action) {
                this.sendAction('sendMessage', button.action);
            } else if(button.url) {
                window.open(button, '_blank');
            }
        }
    },
    layout
});
