import Component from '@ember/component';
import layout from './template';
import { set } from '@ember/object';

export default Component.extend({
  actions: {
    clickedItem(item) {
      if (this.get('radio.disabled')) {
        return;
      }
      this.sendAction('action', item);
      this.get('items').map((other) => {
        if (other != item) {
          set(other, 'hidden', true);
        } else {
          set(other, 'active', true);
        }
      });
      this.set('radio.disabled', true);
    }
  },
  layout

});
