import Component from '@ember/component';
import layout from './template';

export default Component.extend({
  tagName:'div',
  classNames:['oswald-bubble-left'],
  message:null,
  layout,
});
