import Component from '@ember/component';
import layout from './template';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: "a",
  classNames: ['message__link'],
  attributeBindings: ['href', 'target'],
  href: computed.alias('message.url'),
  target: "_blank",
  layout
});
