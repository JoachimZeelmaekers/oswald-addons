oswald-addon
==============================================================================

This addon is used to implement all supported response types of messages coming from [Oswald](https://www.oswald.ai).

Installation
------------------------------------------------------------------------------

```
ember install oswald-addon
```

Explanation
------------------------------------------------------------------------------


### Supported message types:

| Supported types       | Component                 |
| --------------------- |---------------------------|
| Text                  | oswald/text-message       |
| Url (links)           | oswald/url-message        |
| Button                | oswald/button-message     |
| Image                 | oswald/image-message      |
| Video                 | oswald/video-message      |
| Map                   | oswald/map-message        |
| Carousel              | oswald/carousel-message   |
| Json-pretty           | oswald/json-pretty-message|

If the type of the message is not known by the message-balloon, the type will be shown as a [JSON-pretty message](https://www.npmjs.com/package/ember-json-pretty).

Usage
------------------------------------------------------------------------------

Using the diffrent responsetypes is simple by using the `oswald messenger` component in Ember.js.

> In `template.hbs`

```
{{ oswald-messenger allResults=allResults chatbot=chatbot sendMessage=(action "sendMessage")}}
```
> `allResults` is an array of objects. These will be returned by the chat-API of Oswald.

> The `chatbot` has this format:
```
{
  "id":"exampleId",
  "token":"exampleToken",
  "welcomeCommand":false,
  "defaultLocale":"nl",
  "widget": {
    "primaryColor":"rgb(104, 102, 199)",
    "secondaryColor":"rgb(219, 219, 219)"
  }
}
```
> The Action of the component will send the message of a message for example a quick reply or button.

## Example template for a widget in your application

```
<div class="oswald-box primaryColor">
  <div class="oswald-title">
    <div class="oswald-pic">
      <img id="oswaldguy" src={{iconUrl}} width="35"/> 
    </div>
    <p class="sub-title">
      {{subtitle}}
    </p>
    <h3 class="main-title">
      {{title}}
    </h3>
  </div>
  {{ oswald-messenger
    allResults=allResults
    chatbot=chatbot
    sendMessage=(action "sendMessage")
  }}
  <div class="oswald-input">
    <div class="input">
      {{
        input id="message"
        value=message 
        placeholder=placeHolder 
        autofocus="" 
        type=inputType 
        enter=(action "sendMessage" message) 
      }}
      <div class="oswald-button primaryColor" {{action "sendMessage" message}}>
        <i class="fa fa-paper-plane"></i>
      </div>
    </div>
  </div>
</div>
```

# Important

Only if font-awesome is used in your ember application.
```
"dependencies": {
    "ember-font-awesome": "^4.0.0-rc.4"
  },
```
> Remove the `ember-font-awesome` from `devDependencies` and add it to `dependencies`.